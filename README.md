
## GoPets v0.1 (initial version)

### A Pets platform for selling and buying pets, exclusively for and under rights of GoPets team

*Made by [Igor M. Penaque]*

## Get Started

### 1. System Requirements

* Globally installed [node](https://nodejs.org/en/)

* Globally installed [react-native CLI](https://facebook.github.io/react-native/docs/getting-started.html)


### 2. Installation

On the command prompt run the following commands

```sh
$ npm install
  or
  yarn
```

### Run on iOS

 * Opt #1:
 	* Run `npm start` in your terminal
	* Scan the QR code in your Expo app or just press A and run through emulator or USB
 * Opt #2:
	* Run `npm run ios` in your terminal

### Run on Android

  * Opt #1:
	* Run `npm start` in your terminal
	* Scan the QR code in your Expo app or just press A and run through emulator or USB (ADB)
  * Opt #2:
	* Run `npm run android` in your terminal
