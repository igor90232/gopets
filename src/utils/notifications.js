import { Toast } from "native-base";

export const showToast = (message, type = null) => {
  Toast.show({
    text: message,
    duration: 2000,
    position: "top",
    type
  });
};
