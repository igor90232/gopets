
export const required = value => (value && value.length > 0 ? undefined : "Required");

export const maxLength = max => value => {
	return value && value.length > max
		? `No máximo ${max} caracteres`
		: undefined;
};

export const minLength = min => value => {
	return value && value.length < min
		? `No mínimo ${min} caracteres`
		: undefined;
};

export const email = value => {
	return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
		? "Invalid email address"
		: undefined;
};
export const alphaNumeric = value => {
	return value && /[^a-zA-Z0-9 ]/i.test(value)
		? "Only alphanumeric characters"
		: undefined;
};
