export function turnLoading(turnTo?: boolean) {
	return dispatch => {
		dispatch({
			type: "TURN_LOADING",
			isLoading: turnTo,
		});
	};
}
