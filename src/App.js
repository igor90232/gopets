// @flow
import * as React from "react";
import { Root } from "native-base";
import Loading from "./components/Loading";
import { connect } from "react-redux";

import StackNavigator from './router'

export interface Props {
  isLoading: boolean;
}
export interface State {}
class BaseApp extends React.Component<Props, State> {
  render() {
    const { isLoading } = this.props
    return <Root>
        <StackNavigator />
        <Loading loading={isLoading} />
    </Root>;
  }
}

const mapStateToProps = state => ({
  isLoading: state.appReducer.isLoading,
});
export default connect(mapStateToProps, {})(BaseApp);
