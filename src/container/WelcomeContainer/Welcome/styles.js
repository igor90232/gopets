import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
	container: {
		backgroundColor: "#FFFFFF",
	},
	title: {
		color:'#20c0ff',
		fontSize:30,
		fontWeight:'bold',
		textAlign:'center'
	},
	text: {
		color: '#C7C7C7',
		textAlign:'center'
	},
	buttons: {
		flexDirection:'row',
		marginTop:35
	},
	btn: {
		flex:1,
		marginRight:8,
	},
	btn2: {
		flex:1,
		marginLeft:8,
	}
});
export default styles;
