import * as React from "react";
import {
	View,
	Container,
	Header,
	Title,
	Content,
	Text,
	Button,
	Icon,
	Left,
	Right,
	Body
} from "native-base";
import { Image } from "react-native";
import styles from "./styles";
export interface Props {
	navigation: any;
}
export interface State {}
class Welcome extends React.Component<Props, State> {
	render() {
		const { navigation } = this.props;
		const param = navigation.state.params;
		return (
			<Container style={styles.container}>
				<View
					style={{ padding: 20, flex: 1, justifyContent: "flex-end" }}
				>
					<Text style={styles.title}>GoPets</Text>
					<Text style={styles.text}>
						Lorem Ipsum is simply dummy text of the printing and
						typesetting industry.{" "}
					</Text>
					<View style={styles.buttons}>
						<Button
							style={styles.btn}
							block
							primary
							onPress={() => {
								navigation.navigate("Login");
							}}
						>
							<Text>Entrar</Text>
						</Button>
						<Button
							style={styles.btn2}
							block
							bordered
							onPress={() => {
								navigation.navigate("Register");
							}}
						>
							<Text>Cadastre-se</Text>
						</Button>
					</View>
				</View>
			</Container>
		);
	}
}

export default Welcome;
