// @flow
import * as React from "react";
import Welcome from "./Welcome";
export interface Props {
	navigation: any;
}
export interface State {}
export default class WelcomeContainer extends React.Component<Props, State> {
	render() {
		return <Welcome navigation={this.props.navigation} />;
	}
}
