import { showToast } from "@utils/notifications"
import { API_ENDPOINT } from "@utils/request"

export const onLogin = (props) => values => {
  props.turnLoading(true)
  // Request of login
  fetch(`${API_ENDPOINT}/login`, {
    method: "POST",
    body: {}
  })
    .then(response => response.json())
    .then(res => {
      // Set auth
      props.set
      props.turnLoading(false)
      this.props.navigation.navigate("Drawer");
    })
    .catch(err => {
      props.turnLoading(false)
      if (err.status === 400) {
        showToast("Usuário e senha incorretos");
      } else {
        showToast("Erro inesperado ao tentar entrar.");
      }
    });
};