import * as React from "react";
import { Item, Input, Icon, Label, Form } from "native-base";
import {
  required,
  email,
  maxLength,
  minLength,
  alphaNumeric
} from "@utils/appValidators";


import { Field } from "redux-form";
const renderInput = (props) => ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => {
  return (
    <Item floatingLabel>
      <Label>{props.name}</Label>
      <Input
        ref={c => (this.textInput = c)}
        keyboardType={input.name == "email" ? "email-address" : "default"}
        secureTextEntry={input.name === "password" ? true : false}
        {...input}
      />
    </Item>
  );
};

export default form = (
  <Form>
    <Field name="email" component={renderInput({
      name: 'Email'
    })} validate={[email, required]} />
    <Field
      name="password"
      component={renderInput({
        name: 'Senha'
      })}
      validate={[alphaNumeric, minLength(6), maxLength(16), required]}
    />
  </Form>
);