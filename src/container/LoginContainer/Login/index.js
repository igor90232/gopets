import * as React from "react";
import { Image, Platform, ScrollView, TouchableOpacity } from "react-native";
import {
	Container,
	Content,
	Header,
	Body,
	Title,
	Button,
	Text,
	View,
	Icon,
	Footer
} from "native-base";
import theme from "src/theme/variables/platform";

//import styles from "./styles";
export interface Props {
	loginForm: any;
	handleSubmit: any;
}
export interface State {}
class Login extends React.Component<Props, State> {
	render() {
		const { handleSubmit } = this.props;
		return (
			<Container>
				<View
					style={{
						position: "absolute",
						top: 0,
						left: 0,
						width: "100%",
						height: "100%",
						backgroundColor: "#080808",
						justifyContent: "center",
						alignItems: "center"
					}}
				>
					<Image
						style={{ flex: 1, resizeMode: "cover", opacity: 0.7 }}
						source={require("assets/wallpaper01.jpg")}
					/>
				</View>
				<ScrollView>
					<View
						style={{
							alignItems: "center",
							flexDirection: "column",
							paddingTop: 20,
							paddingHorizontal: 15
						}}
					>
						<Image
							source={require("assets/header_logo.png")}
							style={{
								width: 120,
								height: 120,
								resizeMode: "contain"
							}}
						/>
						<View
							style={{
								backgroundColor: "white",
								width: "100%",
								marginTop: 20,
								padding: 20,
								borderRadius: 10
							}}
						>
							{this.props.loginForm}
							<Text
								style={{
									textAlign: "right",
									fontSize: 12,
									color: "#C0C0C0",
									marginTop: 5
								}}
							>
								Esqueceu sua senha?
							</Text>
							<Button
								block
								style={{ marginTop: 30 }}
								onPress={handleSubmit}
							>
								<Text>Entrar</Text>
							</Button>
							<Button
								block
								info
								style={{
									marginTop: 10,
									backgroundColor: "#3c5a99"
								}}
							>
								<Text>Entrar com Facebook</Text>
							</Button>
						</View>
						<TouchableOpacity
							onPress={() => {
								this.props.navigation.navigate("Register");
							}}
						>
							<Text
								style={{
									color: "white",
									fontSize: 14,
									marginTop: 12,
									marginBottom: 15
								}}
							>
								Ainda não tem uma conta?{" "}
								<Text style={{ color: theme.brandPrimary }}>
									Cadastre-se
								</Text>
							</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</Container>
		);
	}
}

export default Login;
