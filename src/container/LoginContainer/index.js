// @flow
import * as React from "react";
import { Item, Input, Icon, Label, Form } from "native-base";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import Login from "./Login";

import { setAuth } from "src/actions/Auth";
import { turnLoading } from "src/actions/App";
import { API_ENDPOINT } from "@utils/request";
import { onLogin } from "./services"
import { showToast } from "@utils/notifications"
import form from "./form"

export interface Props {
  navigation: any;
  valid: boolean;
  turnLoading: Function;
}

class LoginContainer extends React.Component<Props, State> {

  textInput: any;

  handleSubmit() {
    if(this.props.valid) {
      return this.props.handleSubmit(onLogin(this.props))
    } else {
      return () => showToast('Usuário e senhas incorretos', 'danger')
    }
  }

  render() {
    return (
      <Login
        navigation={this.props.navigation}
        loginForm={form}
        handleSubmit={this.handleSubmit()}
      />
    );
  }
}

LoginContainer = reduxForm({
  form: "login"
})(LoginContainer);

const mapDispatchToProps = {
    turnLoading,
    setAuth
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
