import * as React from "react";
import {
	Image,
	Platform,
	ScrollView,
	TouchableOpacity,
	Dimensions
} from "react-native";
import {
	Container,
	Content,
	Header,
	Body,
	Title,
	Button,
	Text,
	View,
	Icon,
	Footer
} from "native-base";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { PhotoGrabber } from "@components/inputs";
import theme from "src/theme/variables/platform";

import styles from "./styles";

const sliderWidth = Dimensions.get("window").width - 100;

const renderSelectCard = data => {
	const colorOverride = data.isSelected ? { color: "white" } : {};
	return (
		<TouchableOpacity
			onPress={data.onPress}
			style={[
				styles.selectCard,
				data.isSelected ? { borderColor: "transparent" } : {}
			]}
		>
			{data.isSelected && (
				<Image
					style={styles.selectBgImg}
					source={require("assets/selectedbg.png")}
				/>
			)}
			<Image style={styles.selectImg} source={data.icon} />
			<Text style={[styles.title2, colorOverride]}>{data.title}</Text>
			<Text style={[styles.mutedDesc, colorOverride]}>{data.desc}</Text>
		</TouchableOpacity>
	);
};

export interface Props {
	navigation: any;
	registerForm: any;
	handleSubmit: any;
}
export interface State {
	currentStep: number;
	sliderIndex: number;
	selectedType: string;
}

class Register extends React.Component<Props, State> {
	state = {
		currentStep: 0,
		sliderIndex: 0,
		selectedType: "",
		profilePhoto: null,
		identityDocument: null,
		operationLicense: null,
	};

	render() {
		const { registerForm, handleSubmit } = this.props;
		const { currentStep, selectedType, sliderIndex } = this.state;

		const renderCarouselStep = step => {
			switch (step.item) {
				case 0:
					return (
						<View>
							<View
								style={{
									alignItems: "center",
									marginTop: 0
								}}
							>
								<PhotoGrabber onChange={(profilePhoto) => this.setState({profilePhoto})}/>
							</View>
							{registerForm("SELLER")}
						</View>
					);
					break;
				case 1:
					return (
						<View style={{ flex: 1 }}>
							<View style={{ alignItems: "center" }}>
								<Text style={styles.formTitle}>
									Por favor envie os documentos
								</Text>
							</View>
							<View
								style={{
									flex: 1,
									flexDirection: "column"
								}}
							>
								<PhotoGrabber
									options={{
										type: "document",
										name: "Documento de Identidade",
										iconImage: require("assets/add_id_document_icon.png")
									}}
									onChange={(identityDocument) => this.setState({identityDocument})}
								/>
								<PhotoGrabber
									options={{
										type: "document",
										name: "Licensa de Operação",
										iconImage: require("assets/add_op_document_icon.png")
									}}
									onChange={(operationLicense) => this.setState({operationLicense})}
								/>
							</View>
						</View>
					);
				case 2:
					return (
						<View style={{ flex: 1 }}>
							<View style={{ alignItems: "center" }}>
								<Text style={styles.formTitle}>
									Dados de acesso
								</Text>
							</View>
							{registerForm("SELLER", 1)}
						</View>
					);
					break;
			}
		};

		// Top header arrow
		const arrowBack = (
			<Button
				transparent
				onPress={() => {
					if (currentStep > 0) {
						this.setState({ currentStep: currentStep - 1 });
					} else {
						this.props.navigation.goBack();
					}
				}}
				style={{
					position: currentStep > 0 ? "absolute" : "relative",
					zIndex: 15,
					top: currentStep > 0 ? -10 : 0
				}}
			>
				<Icon
					style={{
						color: currentStep === 0 ? "#343434" : "white"
					}}
					name="arrow-back"
				/>
			</Button>
		);

		// Steps of main page
		switch (currentStep) {
			case 0:
				return (
					<Container style={{ backgroundColor: "white" }}>
						{arrowBack}
						<View>
							<Text style={styles.title}>Quem é você?</Text>
							<Text style={styles.mutedDesc}>Escolha o tipo</Text>
						</View>
						<View style={styles.selectContainer}>
							{renderSelectCard({
								icon:
									selectedType === "CLIENT"
										? require("assets/buyer-1.png")
										: require("assets/buyer.png"),
								isSelected: selectedType === "CLIENT",
								title: "Sou cliente",
								desc: "Eu quero comprar Pets",
								onPress: () =>
									this.setState({
										selectedType:
											selectedType === "CLIENT"
												? ""
												: "CLIENT"
									})
							})}
							{renderSelectCard({
								icon:
									selectedType === "SELLER"
										? require("assets/seller-1.png")
										: require("assets/seller.png"),
								isSelected: selectedType === "SELLER",
								title: "Sou vendedor",
								desc: "Eu quero vender Pets",
								onPress: () =>
									this.setState({
										selectedType:
											selectedType === "SELLER"
												? ""
												: "SELLER"
									})
							})}
						</View>
						<Button
							block
							disabled={selectedType === ""}
							style={{
								marginBottom: 20,
								marginLeft: 20,
								marginRight: 20
							}}
							onPress={() => this.setState({ currentStep: 1 })}
						>
							<Text>Próximo</Text>
						</Button>
					</Container>
				);
				break;
			case 1:
				return (
					<Container>
						<View style={styles.absoluteContainer}>
							<Image
								style={styles.absoluteImg}
								source={require("assets/wallpaper01.jpg")}
							/>
						</View>
						<ScrollView>
							<View
								style={{
									flex: 1,
									flexDirection: "row",
									alignItems: "center",
									marginTop: 8
								}}
							>
								{arrowBack}
								<Text
									style={{
										color: "white",
										textAlign: "center",
										flex: 1,
										fontSize: 16
									}}
								>
									Eu sou{" "}
									{selectedType === "SELLER"
										? "Vendedor"
										: "Cliente"}
								</Text>
							</View>
							<View style={styles.whiteCard}>

								{selectedType === "CLIENT" && (
									<View style={{flex:1, width:'100%', marginBottom:20}}>
										<View
											style={{
												alignItems: "center",
												marginTop: 0
											}}
										>
											<PhotoGrabber onChange={(profilePhoto) => this.setState({profilePhoto})} />
										</View>
										{registerForm("CLIENT")}
									</View>
								)}

								{selectedType === "SELLER" && (
									<View style={{ flex: 1 }}>
										<Carousel
											ref={c => (this._slider = c)}
											data={[0, 1, 2]}
											renderItem={renderCarouselStep}
											sliderWidth={sliderWidth}
											itemWidth={sliderWidth}
											value={sliderIndex}
											onSnapToItem={index =>
												this.setState({
													sliderIndex: index
												})
											}
										/>

										<Pagination
											dotsLength={3}
											activeDotIndex={sliderIndex}
											carouselRef={this._slider}
											tappableDots={!!this._slider}
											dotColor={theme.brandPrimary}
											inactiveDotColor="#e1e1e1"
											inactiveDotOpacity={1}
											inactiveDotScale={1}
										/>
									</View>
								)}

								<Button
									block
									onPress={() => {
										if(selectedType === 'CLIENT') {
											handleSubmit({
												type:'CLIENT',

											})
										} else {
										if (sliderIndex < 2) {
											this._slider.snapToItem(
												sliderIndex + 1
											);
											this.setState({
												sliderIndex: sliderIndex + 1
											});
										} else {
											handleSubmit()
										}
										}
									}}
								>
									<Text>
										{sliderIndex === 2 || selectedType === 'CLIENT' ? 'Cadastrar'  : 'Próximo'}
									</Text>
								</Button>
							</View>
							<TouchableOpacity
								onPress={() => this.navigation.goBack()}
							>
								<Text
									style={{
										textAlign: "center",
										color: "white",
										marginBottom: 15,
										fontSize: 14
									}}
								>
									Já tem uma conta?{" "}
									<Text
										style={{
											color: theme.brandPrimary,
											fontSize: 14
										}}
									>
										Entre
									</Text>
								</Text>
							</TouchableOpacity>
						</ScrollView>
					</Container>
				);
				break;
		}
	}
}

export default Register;
