import { Dimensions, StyleSheet } from "react-native";

const deviceHeight = Dimensions.get("window").height;

const styles: any = StyleSheet.create({
	title: {
		fontSize: 22,
		color: "#343434",
		textAlign: "center",
		fontWeight: "600"
	},
	title2: {
		fontSize: 20,
		color: "#343434",
		textAlign: "center"
	},
	mutedDesc: {
		fontSize: 14,
		color: "#9D9D9D",
		textAlign: "center"
	},
	selectCard: {
		borderColor: "#E1E1E1",
		borderWidth: 1,
		borderRadius: 12,
		marginTop: 10,
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		overflow: "hidden"
	},
	selectBgImg: {
		position: "absolute",
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width: "100%",
		height: "100%",
		flex: 1,
		backgroundColor: "#1D98DF"
	},
	selectContainer: {
		paddingLeft: 20,
		paddingRight: 20,
		paddingBottom: 20,
		flex: 1
	},
	selectImg: {
		resizeMode: "contain",
		width: 60,
		height: 60
	},

	absoluteContainer: {
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
		backgroundColor: "#080808",
		justifyContent: "center",
		alignItems: "center"
	},
	absoluteImg: {
		flex: 1,
		resizeMode: "cover",
		opacity: 0.7
	},
	whiteCard: {
		backgroundColor: "white",
		flex: 1,
		alignItems: "center",
		padding: 20,
		marginHorizontal: 20,
		marginVertical: 10,
		borderRadius: 8
	},
	formTitle: {
		color: "#343434",
		fontSize: 18,
		textAlign: "center",
		maxWidth: 200
	}
});
export default styles;
