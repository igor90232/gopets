// @flow
import * as React from "react";
import { Item, Input, Icon, Label, Form } from "native-base";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import Register from "./Register";

import { setAuth } from "src/actions/Auth";
import { turnLoading } from "src/actions/App";
import { API_ENDPOINT } from "@utils/request";
import { onRegister } from "./services"
import { showToast } from "@utils/notifications"
import form from "./form"

export interface Props {
  navigation: any;
  valid: boolean;
  turnLoading: Function;
}
export interface State {}

class RegisterContainer extends React.Component<Props, State> {

  textInput: any;

  handleSubmit() {
    if(this.props.valid) {
      return this.props.handleSubmit(onRegister(this.props))
    } else {
      return () => {
        showToast('Preencha todos os campos corretamente!', 'danger')
      }
    }
  }

  render() {
    return (
      <Register
        navigation={this.props.navigation}
        handleSubmit={this.handleSubmit()}
        registerForm={form}
      />
    );
  }
}

RegisterContainer = reduxForm({
  form: "register"
})(RegisterContainer);

const mapDispatchToProps = {
    turnLoading,
    setAuth
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer);
