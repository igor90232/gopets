import { showToast } from "@utils/notifications"
import { API_ENDPOINT } from "@utils/request"

export const onRegister = (props) => values => {
  props.turnLoading(true)
  // Request of register
  const body = {
    
  }
  fetch(`${API_ENDPOINT}/register`, {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
    .then(response => response.json())
    .then(res => {
      // Set auth
      props.set
      props.turnLoading(false)
      this.props.navigation.navigate("Drawer");
    })
    .catch(err => {
      props.turnLoading(false)
      if (err.status === 400) {
        showToast("Usuário e senha incorretos");
      } else {
        showToast("Erro inesperado ao tentar entrar.");
      }
      console.log(err)
    });
};