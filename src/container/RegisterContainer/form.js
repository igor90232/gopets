import * as React from "react";
import {
  View,
  Text,
  Item,
  Input,
  Icon,
  Picker,
  Label,
  Form
} from "native-base";
import {
  required,
  email,
  maxLength,
  minLength,
  alphaNumeric
} from "@utils/appValidators";

import { Field } from "redux-form";
const renderInput = props => ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => {
  return (
    <Item floatingLabel>
      <Label>{props.name}</Label>
      <Input
        ref={c => (this.textInput = c)}
        keyboardType={props.type === "email" ? "email-address" : "default"}
        secureTextEntry={props.type === "password" ? true : false}
        {...input}
      />
    </Item>
  );
};

const renderPicker = props => ({
  input: { onChange, value, ...inputProps },
  children,
  ...pickerProps
}) => (
  <Item picker stackedLabel>
    <Label>{props.name}</Label>
    <Picker
      selectedValue={value}
      onValueChange={value => onChange(value)}
      iosIcon={<Icon name="arrow-down" />}
      style={{ width: "100%" }}
      {...inputProps}
      {...pickerProps}
    >
      {children}
    </Picker>
  </Item>
);

export default (form = (type, step = 0) => {
  if (type === "CLIENT") {
    return (
      <Form>
        <Field
          name="name"
          component={renderInput({
            name: "Nome"
          })}
          validate={[required]}
        />
        <Field
          name="email"
          component={renderInput({
            name: "Email",
            type: "email"
          })}
          validate={[email, required]}
        />
        <Field
          name="password"
          component={renderInput({
            name: "Senha",
            type: "password"
          })}
          validate={[alphaNumeric, minLength(6), maxLength(16), required]}
        />
        <Field
          name="confirm_password"
          component={renderInput({
            name: "Confirmar senha",
            type: "password"
          })}
          validate={[alphaNumeric, minLength(6), maxLength(16), required]}
        />
      </Form>
    );
  } else {
    switch (step) {
      case 0:
        return (
          <Form>
            <Field
              name="name"
              component={renderInput({
                name: "Nome"
              })}
              validate={[required]}
            />
            <Field
              name="establishment_name"
              component={renderInput({
                name: "Nome do Estabelecimento"
              })}
              validate={[required]}
            />
            <Field
              name="identityNumber"
              component={renderInput({
                name: "CPF ou CNPJ"
              })}
              validate={[required]}
            />

            <Field
              name="business_type"
              component={renderPicker({
                name: "Tipo de Negócio"
              })}
              iosHeader="Selecione uma opção"
              mode="dropdown"
              validate={[required]}
            >
              <Picker.Item label="Selecione uma opção" value="" />
              <Picker.Item label="Canil" value="kennel" />
              <Picker.Item label="Gatil" value="cattery" />
              <Picker.Item label="ONG" value="ong" />
            </Field>
          </Form>
        );
        break;
      case 1:
        return (
          <Form>
            <Field
              name="email"
              component={renderInput({
                name: "Email",
                type: "email"
              })}
              validate={[email, required]}
            />
            <Field
              name="password"
              component={renderInput({
                name: "Senha",
                type: "password"
              })}
              validate={[alphaNumeric, minLength(6), maxLength(16), required]}
            />
            <Field
              name="confirm_password"
              component={renderInput({
                name: "Confirmar senha",
                type: "password"
              })}
              validate={[alphaNumeric, minLength(6), maxLength(16), required]}
            />
          </Form>
        );
        break;
    }
  }
});
