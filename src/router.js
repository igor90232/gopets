import { createStackNavigator, createDrawerNavigator } from "react-navigation";
import Login from "./container/LoginContainer";
import Register from "./container/RegisterContainer";
import Welcome from "./container/WelcomeContainer";
import Home from "./container/HomeContainer";
import BlankPage from "./container/BlankPageContainer";
import Sidebar from "./container/SidebarContainer";

const Drawer = createDrawerNavigator(
  {
    Home: { screen: Home }
  },
  {
    initialRouteName: "Home",
    contentComponent: props => <Sidebar {...props} />
  }
);

export default StackNavigator = createStackNavigator(
  {
    Welcome: { screen: Welcome },
    Login: { screen: Login },
    Register: { screen: Register },
    BlankPage: { screen: BlankPage },
    Drawer: { screen: Drawer }
  },
  {
    initialRouteName: "Register",
    headerMode: "none"
  }
);