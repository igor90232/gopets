import * as React from "react";
import { Text, View, Spinner } from "native-base";
import theme from 'src/theme/variables/platform'
export interface Props {
	loading: any;
}
export interface State {}
export default class Loading extends React.Component<Props, State> {
	render() {
		const { loading } = this.props;
		if (!loading) {
			return null;
		}
		return (
			<View
				style={{
					backgroundColor: "rgba(255, 255, 255, 0.8)",
					position: "absolute",
					flex: 1,
					top: 0,
					left: 0,
					right: 0,
					bottom: 0,
					alignItems:'center',
					justifyContent:'center',
					width: "100%",
					height: "100%"
				}}
			>
			<Spinner color={theme.brandPrimary} />
			</View>
		);
	}
}