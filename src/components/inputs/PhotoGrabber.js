import * as React from "react";
import { Image, TouchableOpacity } from "react-native";
import { ActionSheet, Icon, Text, View, Spinner } from "native-base";
import theme from "src/theme/variables/platform";
import { Permissions, ImagePicker } from "expo";
import { showToast } from "@utils/notifications";

export interface Props {
	loading: any;
	options: object;
}

export interface State {
	currentImage: string;
}

const size = 100;
export default class PhotoGrabber extends React.Component<Props, State> {
	state = {
		currentImage: null
	};

	grabImage = async useCamera => {
		const status = await Permissions.askAsync(
			Permissions.CAMERA,
			Permissions.CAMERA_ROLL
		);
		console.log("Permissions status for camera", status);

		const pOptions = this.props.options;

		let options;
		if (pOptions && pOptions.type === "document") {
			options = {
				allowsEditing: true
			};
		} else {
			options = {
				allowsEditing: true,
				aspect: [1, 1]
			};
		}

		let currentImage;

		try {
			if (useCamera) {
				currentImage = await ImagePicker.launchCameraAsync(options);
			} else {
				currentImage = await ImagePicker.launchImageLibraryAsync(
					options
				);
			}

			this.props.onChange && this.props.onChange(currentImage)

			this.setState({ currentImage });
		} catch (err) {
			showToast(
				"Verifique as permissões de câmera e galeria do aplicativo.",
				"error"
			);
		}
	};

	render() {
		const { options } = this.props;
		const { currentImage } = this.state;

		const onPress = () => {
			ActionSheet.show(
				{
					options: [
						{ text: "Selecionar na galeria", icon: "photos" },
						{ text: "Tirar foto", icon: "camera" },
						{ text: "Cancelar" }
					],
					cancelButtonIndex: 2,
					title: "Foto de perfil"
				},
				buttonIndex => {
					if (buttonIndex === 0) {
						this.grabImage(false);
					} else if (buttonIndex === 1) {
						this.grabImage(true);
					}
				}
			);
		};

		if (options && options.type === "document") {
			return (
				<TouchableOpacity
					style={{
						flex: 1,
						height: "100%",
						borderRadius: 6,
						backgroundColor: "#f2f2f2",
						justifyContent: "center",
						alignItems: "center",
						marginTop: 10
					}}
					onPress={onPress}
				>
					{currentImage ? (
						<Image
							source={{ uri: currentImage.uri }}
							style={{
								position: "absolute",
								top: 0,
								left: 0,
								width: "100%",
								height: "100%",
								resizeMode: "cover"
							}}
						/>
					) : (
						<Image
							source={options.iconImage}
							style={{
								width: 60,
								height: 60,
								resizeMode: "contain"
							}}
						/>
					)}
					<Text
						style={currentImage ? {
							color:'white',
							fontSize: 16,
							textShadowColor: 'rgba(0, 0, 0, 0.75)',
							textShadowOffset: {width: 1, height: 1},
							textShadowRadius: 10
						} : {
							marginTop: 6,
							color: "#a7a7a7",
							fontSize: 16
						}}
					>
						{options.name}
					</Text>
				</TouchableOpacity>
			);
		}

		return (
			<TouchableOpacity
				style={{
					height: size,
					borderRadius: size,
					width: size,
					backgroundColor: "#f2f2f2",
					justifyContent: "center",
					alignItems: "center"
				}}
				onPress={onPress}
			>
				{currentImage ? (
					<Image
						source={{ uri: currentImage.uri }}
						style={{
							position: "absolute",
							top: 0,
							left: 0,
							width: "100%",
							height: "100%",
							borderRadius: size,
							resizeMode: "cover"
						}}
					/>
				) : (
					<Icon
						name="person"
						style={{
							color: "#dbdbdb",
							fontSize: 50
						}}
					/>
				)}
				<Image
					style={{
						position: "absolute",
						bottom: 0,
						right: 0,
						width: 36,
						height: 36,
						resizeMode: "contain"
					}}
					source={require("assets/upload.png")}
				/>
			</TouchableOpacity>
		);
	}
}
