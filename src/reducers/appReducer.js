const initialState = {
	isLoading: false,
};

export default function(state: any = initialState, action: Function) {
	if (action.type === "TOGGLE_LOADING") {
		return {
			...state,
			isLoading: !state.isLoading
		};
	}
	if (action.type === "TURN_LOADING") {
		return {
			...state,
			isLoading: action.isLoading
		};
	}
	return state;
}
