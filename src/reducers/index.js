import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import appReducer from "./appReducer";
import homeReducer from "../container/HomeContainer/reducer";

export default combineReducers({
	form: formReducer,
	appReducer,
	homeReducer,
});
